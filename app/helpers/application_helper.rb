#encoding: utf-8
module ApplicationHelper
  def frendly_boolean(value)
    if value
      'Sim'
    else
      'Não'
    end
  end

  def format_date_br(date)
    "#{date.day}/#{date.month}/#{date.year}" unless date.nil?
  end

  def schedules_select
    a = [
      ['06:00 - 07:00', 0],
      ['07:00 - 08:00', 1],
      ['08:00 - 09:00', 2],
      ['09:00 - 10:00', 3],
      ['10:00 - 11:00', 4],
      ['11:00 - 12:00', 5],
      ['12:00 - 13:00', 6],
      ['13:00 - 14:00', 7],
      ['14:00 - 15:00', 8],
      ['15:00 - 16:00', 9],
      ['16:00 - 17:00', 10],
      ['17:00 - 18:00', 11],
      ['18:00 - 19:00', 12],
      ['19:00 - 20:00', 13],
      ['20:00 - 21:00', 14],
      ['21:00 - 22:00', 15]]
  end
end
