class AlocationPermanent < ActiveRecord::Base
  belongs_to :space
  belongs_to :semester
  attr_accessible :deleted, :schedule, :week_day, :space_id, :semester_id

  validates :schedule, presence: true
  validates :week_day, presence: true

end
