class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :siape
      t.string :password
      t.string :email
      t.string :phone
      t.boolean :active
      t.references :institution
      t.references :sector
      t.references :role
      t.references :user_type
      t.boolean :deleted
      t.string :cpf
      t.string :registration


      t.timestamps
    end
    add_index :users, :institution_id
    add_index :users, :sector_id
    add_index :users, :role_id
  end
end
