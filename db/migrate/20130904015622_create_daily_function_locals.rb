class CreateDailyFunctionLocals < ActiveRecord::Migration
  def change
    create_table :daily_function_locals do |t|
      t.references :daily
      t.references :function
      t.references :local

      t.timestamps
    end
    add_index :daily_function_locals, :daily_id
    add_index :daily_function_locals, :function_id
    add_index :daily_function_locals, :local_id
  end
end
