class CreateRecesses < ActiveRecord::Migration
  def change
    create_table :recesses do |t|
      t.date :begin
      t.date :end
      t.string :description
      t.boolean :deleted

      t.timestamps
    end
  end
end
