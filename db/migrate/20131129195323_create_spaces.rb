class CreateSpaces < ActiveRecord::Migration
  def change
    create_table :spaces do |t|
      t.string :name
      t.integer :capacity
      t.text :observations
      t.integer :size
      t.float :price_local
      t.boolean :deleted
      t.references :location_type

      t.timestamps
    end
    add_index :spaces, :location_type_id
  end
end
