class CreateAlocationPermanents < ActiveRecord::Migration
  def change
    create_table :alocation_permanents do |t|
      t.integer :schedule
      t.integer :week_day
      t.references :space
      t.boolean :deleted
      t.references :semester

      t.timestamps
    end
    add_index :alocation_permanents, :space_id
    add_index :alocation_permanents, :semester_id
  end
end
