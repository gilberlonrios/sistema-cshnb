require 'test_helper'

class DailyFunctionLocalsControllerTest < ActionController::TestCase
  setup do
    @daily_function_local = daily_function_locals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:daily_function_locals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create daily_function_local" do
    assert_difference('DailyFunctionLocal.count') do
      post :create, daily_function_local: {  }
    end

    assert_redirected_to daily_function_local_path(assigns(:daily_function_local))
  end

  test "should show daily_function_local" do
    get :show, id: @daily_function_local
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @daily_function_local
    assert_response :success
  end

  test "should update daily_function_local" do
    put :update, id: @daily_function_local, daily_function_local: {  }
    assert_redirected_to daily_function_local_path(assigns(:daily_function_local))
  end

  test "should destroy daily_function_local" do
    assert_difference('DailyFunctionLocal.count', -1) do
      delete :destroy, id: @daily_function_local
    end

    assert_redirected_to daily_function_locals_path
  end
end
